package com.wlhlearn.ormdemo2.entity;

import javax.persistence.Table;
import java.util.Date;

/**
 * @Author: wlh
 * @Date: 2019/5/21 13:56
 * @Version 1.0
 * @despricate:learn
 */
@Table(name = "user")
public class User {

    private Integer id ;
    private String loginName ;
    private String password ;
    private String name ;
    private Date birthday ;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }


    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", loginName='" + loginName + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", birthday=" + birthday +
                '}';
    }
}
