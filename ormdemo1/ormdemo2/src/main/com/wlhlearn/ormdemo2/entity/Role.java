package com.wlhlearn.ormdemo2.entity;

import javax.persistence.Table;

/**
 * @Author: wlh
 * @Date: 2019/5/21 14:49
 * @Version 1.0
 * @despricate:learn
 */
@Table(name="role")
public class Role {

    private  Integer id ;
    private  String name ;
    private  Integer status ;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Role{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", status=" + status +
                '}';
    }
}
