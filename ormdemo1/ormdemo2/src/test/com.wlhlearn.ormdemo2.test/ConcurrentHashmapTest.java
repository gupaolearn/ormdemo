package com.wlhlearn.ormdemo2.test;

import java.util.HashMap;
import java.util.UUID;

/**
 * @Author: wlh
 * @Date: 2019/5/22 9:28
 * @Version 1.0
 * @despricate:learn
 */
public class ConcurrentHashmapTest {

    public static void main(String[] args) {
        final HashMap<String, String> map = new HashMap<String, String>();
        for (int i = 0; i < 1000; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    map.put(UUID.randomUUID().toString(), "");
                }
            }).start();
            System.out.println("----------------------");
            System.out.println(map.size());
            for (String str : map.keySet()) {
                System.out.println(str);
            }
            System.out.println("----------------------");
        }
    }
}
