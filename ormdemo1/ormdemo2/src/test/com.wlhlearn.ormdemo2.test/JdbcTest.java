package com.wlhlearn.ormdemo2.test;

import javax.persistence.Column;
import javax.persistence.Table;
import com.wlhlearn.ormdemo2.entity.User;

import java.lang.reflect.Field;
import java.sql.*;
import java.util.*;


/**
 * @Author: wlh
 * @Date: 2019/5/20 17:37
 * @Version 1.0
 * @despricate:learn
 */
public class JdbcTest {
    public static void main(String[] args) throws Exception {
        List<?> result = select(new User());
        System.out.println(Arrays.toString(result.toArray()));

    }

    private static List<?> select(Object condition) throws Exception {
        List<Object> result = new ArrayList<>();
        Connection connection = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;
        try {
            //1 加载驱动
            Class.forName("com.mysql.jdbc.Driver");
            //2 建立连接
            connection = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/test?characterEncoding=UTF-8", "root", "root");
            Class<?> entityClass = condition.getClass();
            //根据类名找属性名
            Map<String, String> columnMapper = new HashMap<String, String>();
//根据属性名找字段名
            Map<String, String> fieldMapper = new HashMap<String, String>();
            Field[] fields = entityClass.getDeclaredFields();
            for (Field field : fields) {
                field.setAccessible(true);
                String fieldName = field.getName();
                if (field.isAnnotationPresent(Column.class)) {
                    Column column = field.getAnnotation(Column.class);
                    String columnName = column.name();
                    columnMapper.put(columnName, fieldName);
                    fieldMapper.put(fieldName, columnName);
                } else {
                    //默认就是字段名属性名一致
                    columnMapper.put(fieldName, fieldName);
                    fieldMapper.put(fieldName, fieldName);
                }
            }


            //3、 创建语句集
            Table table = entityClass.getAnnotation(Table.class);
            String sql = "select * from " + table.name();
            StringBuffer where = new StringBuffer(" where 1=1 ");
            for (Field field : fields) {
                Object value = field.get(condition);
                if (null != value) {
                    if (String.class == field.getType()) {
                        where.append(" and " + fieldMapper.get(field.getName()) + " = '" + value + "'");
                    } else {
                        where.append(" and " + fieldMapper.get(field.getName()) + " = " + value + "");
                    } //其他的， 在这里就不一一列举， 下半截我们手写 ORM 框架会完善
                }
            }
            System.out.println(sql + where.toString());
            pstm = connection.prepareStatement(sql + where.toString());
//4、 执行语句集
            rs = pstm.executeQuery();
//元数据？
//保存了处理真正数值以外的所有的附加信息
            int columnCounts = rs.getMetaData().getColumnCount();
            while (rs.next()) {
                Object instance = entityClass.newInstance();
                for (int i = 1; i <= columnCounts; i++) {
//实体类 属性名， 对应数据库表的字段名
//可以通过反射机制拿到实体类的说有的字段
//从 rs 中取得当前这个游标下的类名
                    String columnName = rs.getMetaData().getColumnName(i);
//有可能是私有的
                    Field field = entityClass.getDeclaredField(columnMapper.get(columnName));
                    field.setAccessible(true);
                    field.set(instance, rs.getObject(columnName));
                }
                result.add(instance);
            } //5、 获取结果集
        } catch (Exception e) {
            e.printStackTrace();
        } //6、 关闭结果集、 关闭语句集、 关闭连接
        finally {
            try {
                rs.close();
                pstm.close();
                connection.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }

      /*  List<User> result = new ArrayList<User>();

        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {


            String sql = "select  * from user";
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {

                // 第一版    属性太多时 这样写会累死码农
               *//* User user=new User() ;
                user.setId(resultSet.getInt("id"));
                user.setLoginName(resultSet.getString("loginName"));
                user.setName(resultSet.getString("name"));
                //  .......
                 result.add(user);
                *//*


                // 第二版  优化   但是此时优化还是不够的
                //  换成其他的实体  那么这个方法就失效了  还是会有很多重复的工作
                // List<User> res=converToEntity(resultSet,resultSet.getRow());


                // 第三版优化  实体与数据库字段名称之间存在对应关系  可以通过反射获取实体上的属性名称   然后对应数据库的字段名


            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
                statement.close();
                resultSet.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
        return result;*/


    private static List<User> converToEntity(ResultSet resultSet, int index) {
        List<User> result = new ArrayList<>();
        User user = new User();
       /* user.setId(resultSet.getInt("id"));
        user.setLoginName(resultSet.getString("loginName"));
        user.setName(resultSet.getString("name"));*/
        //  .......
        result.add(user);
        return result;
    }
}
